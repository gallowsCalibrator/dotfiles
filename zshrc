# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored
zstyle :compinstall filename "~/.zshrc"

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory beep notify PROMPT_SUBST
unsetopt autocd
bindkey -v
# End of lines configured by zsh-newuser-install

# Set up SSH keychain
if [ -x "$(command -v keychain)" ]; then
    eval $(keychain --eval --quiet id_rsa)
fi

export EDITOR="vim"
if [ -f ~/.config/tmuxinator.zsh ]; then
    source ~/.config/tmuxinator.zsh
fi

# PROMPT -------------------------
# prompt will now show git dirty state
if [ -e ~/.dot/git-prompt.sh ]; then
    source ~/.dot/git-prompt.sh
    export GIT_PS1_SHOWDIRTYSTATE=1
    precmd () { __git_ps1 "%F{cyan}%n%F{blue}" " %F{yellow}%c %f" " (%s)" }
fi

# WAL COLORS ----------------------
# Import colorscheme from 'wal'
if [ -x "$(command -v wal)" ]; then
    (wal -r &)
fi

# ANDROID STUDIO ------------------
export ANDROID_HOME=/opt/android-sdk

# MPD HOST ------------------------
export MPD_HOST=~/.mpd/socket

# set appropriate makeflags for multicore processors
NB_CORES=$(grep -c '^processor' /proc/cpuinfo)
export MAKEFLAGS="-j$((NB_CORES+1)) -l${NB_CORES}"

# rbenv
if [ -f ~/.rbenv/bin/rbenv ]; then
    export PATH="$PATH:$HOME/.rbenv/bin"
    eval "$(rbenv init -)"
fi

# attempt to connect to tmux session, create new one otherwise
if [ -x "$(command -v tmux)" ]; then
    if ! [ -n "${TMUX}" ]; then
        tmux new -As base
    fi
fi

# add personal bin directory to PATH
if [ ! -d ~/.dot/bin ]; then
    mkdir ~/.dot/bin
fi
export PATH="$PATH:$HOME/.dot/bin"

# load custom aliases and functions
source ~/.dot/aliases_functions.sh
