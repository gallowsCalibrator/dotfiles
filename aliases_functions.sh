source $(dirname "$0")/vars.sh

exit() {
    if [ -z $TMUX ]; then
        builtin exit
    else
        tmux detach -P
    fi
}

server () {
    case $1 in
        "console")
            ssh root@${DOMAIN} -t "tmux attach"
            ;;
        "stats")
            ssh root@${DOMAIN} -t "gotop"
            ;;
        *)
            ssh root@${DOMAIN}
            ;;
    esac
}

github() {
    if (( $# >= 2 )) then
        git clone https://github.com/$1/$2 $3
    else
        echo "Insufficient parameters!"
        echo "github [username] [repo] <directory>"
    fi
}

alias ls="ls --color"
alias dkm="sudo $(history -p !!)"
alias msfconsole="msfconsole --quiet -x \"db_connect ${USER}@msf\""
alias song="python -m youtube_to_mpd -s"
alias rmpc="ncmpcpp -h ${MPDPASS}@${DOMAIN}"
alias tunnel="ssh -D 6969 -f -C -q -N root@t.${DOMAIN}"
alias nf="neofetch --ascii ${HOME}/.dot/roxysmall.txt --gtk2 off --gtk3 off"

unset MPDPASS
