# Roxy's dotfiles
Intended for use with zsh


NOTE: zshrc expects aliases_functions.sh to be at ~/.dot/ so make sure you clone it into there

you can do `ln -sv ~/.dot/zshrc ~/.zshrc` to symlink zshrc to where zsh expects it to be

if you use the MPD alias/server() function make sure you have a file named vars.sh

most things in zshrc are behind if statements checking the path for appropriate programs, these are

* keychain (SSH authentication agent)
* tmuxinator (save/loads complex tmux sessions to project files)
* wal (colorscheme loader)
* rbenv (ruby version manager, better than rvm)
* tmux (terminal multiplexer)
* git-prompt.sh (adds git repo status to $PS1 if available)

Most of these should be available in your distros package manager, you can get git-prompt.sh by doing

`wget https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh -O ~/.dot/git-prompt.sh`
